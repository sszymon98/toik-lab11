package com.szymonbilinski.demo.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
public class MovieDto {
    private int movieId;
    private String title;
    private int year;
    private String image;
}
