package com.szymonbilinski.demo.controller;

import com.szymonbilinski.demo.dto.MovieDto;
import com.szymonbilinski.demo.repository.MovieRepository;
import com.szymonbilinski.demo.service.MovieServie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
public class MovieController {
    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieServie movieServie;

    @GetMapping("/movies")
    public ResponseEntity<Map<String,List<MovieDto>>> getMovies(){
        List<MovieDto> list = movieRepository.readFile();
        Map<String, List<MovieDto>> map = movieServie.convertLinesToMovies(list);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }
}
