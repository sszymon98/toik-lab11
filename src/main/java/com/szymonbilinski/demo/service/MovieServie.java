package com.szymonbilinski.demo.service;

import com.szymonbilinski.demo.dto.MovieDto;

import java.util.List;
import java.util.Map;

public interface MovieServie {
    Map<String, List<MovieDto>> convertLinesToMovies(List<MovieDto> lines);
}
