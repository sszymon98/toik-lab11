package com.szymonbilinski.demo.service;

import com.szymonbilinski.demo.dto.MovieDto;
import com.szymonbilinski.demo.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class MovieServiceImpl implements MovieServie{
    @Autowired
    private MovieRepository movieRepository;

    @Override
    public Map<String,List<MovieDto>> convertLinesToMovies(List<MovieDto> lines) {
        List<MovieDto> movieDtoList = movieRepository.readFile().stream()
                .map(movieDto -> MovieDto.builder()
                        .movieId(movieDto.getMovieId())
                        .title(movieDto.getTitle())
                        .year(movieDto.getYear())
                        .image(movieDto.getImage())
                        .build())
                .collect(Collectors.toList());

        Map<String, List<MovieDto>> movies = new HashMap<>();
        movies.put("movies",movieDtoList);

        return movies;
    }
}
