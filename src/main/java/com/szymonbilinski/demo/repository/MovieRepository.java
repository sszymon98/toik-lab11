package com.szymonbilinski.demo.repository;

import com.szymonbilinski.demo.dto.MovieDto;

import java.util.List;
import java.util.Map;

public interface MovieRepository {
    List<MovieDto> readFile();
}
