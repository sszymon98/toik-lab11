package com.szymonbilinski.demo.repository;

import com.szymonbilinski.demo.dto.MovieDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MovieReposotoryImpl implements MovieRepository {
    private static final Logger LOGGER = LoggerFactory.getLogger(MovieReposotoryImpl.class);
    private List<String> lines;

    public MovieReposotoryImpl() {
        LOGGER.info("*** Dziala konstruktor CsvOperationsComponentImpl() ***");
        lines = new ArrayList<>();
    }

    @Override
    public List<MovieDto> readFile() {
        List<MovieDto> listMovies = new ArrayList<>();
        listMovies.add(MovieDto.builder().movieId(1).title("XDDD").year(2001).image("https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg").build());
        listMovies.add(MovieDto.builder().movieId(2).title("Film").year(1993).image("https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg").build());
        return listMovies;
    }
}
